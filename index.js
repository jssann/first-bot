const { Client } = require('discord.js');
const { config } = require('dotenv');

const client = new Client();
config({
  path: `${__dirname}/.env`
});

client.on('ready', () => {
  console.log(`Connected as ${client.user.tag}`);

  
  console.log("Servers:");
  // console.log(client.guilds);
  // client.guilds.forEach(guild => {
  //   console.log(` - ${guild.name}`);
  // });
});

client.on('message', message => {
  /**
   * Prevent bot from responding to its own messages
   */
  if (message.author === client.user) return;

  if (message.content.startsWith('!')) {
    processCommand(message);
  }
});

const processCommand = message => {
  
  // Remove the leading exclamation mark
  const cmd = message.content.substr(1);

  // Split the message up in to pieces for each space
  const splitCommand = cmd.split(' ');

  // The first word directly after the exclamation is the command
  const primary =  splitCommand[0];

  // All other words are arguments/parameters/options for the command
  const arguments = splitCommand.slice(1);

  console.log(`Command received: ${primary}`);

  switch(primary) {
    case 'help': {
      helpCommand(arguments, message);
      break;
    }
    default:
      message.channel.send('I don\'t understand the command. Try `!help`');
  }

};

const helpCommand = (arguments, message) => {
  // if (arguments.length > 0) {
  //     receivedMessage.channel.send(`Fuck you ${message.author.toString()}`);
  // } else {
  //     receivedMessage.channel.send('I\'m not sure what you need help with. Try `!help [topic]`');
  // }
  message.channel.send(`Fuck you ${message.author.toString()}`);
}


client.login(process.env.TOKEN);